# Overview #
Experimental project looking at Razor Pages

### How do I get set up? ###

* Install .NET Core SDK (14th Dec 2017 currently .NET Core SDK is 2.1.3 with .NET Core Runtime of 2.0.4) from any of:
* https://docs.microsoft.com/en-us/aspnet/core/tutorials/razor-pages/razor-pages-start
* https://www.microsoft.com/net/learn/get-started/windows
* https://github.com/dotnet/core/blob/master/release-notes/download-archive.md


* VS2017 with latest updates
* Create a database in (localdb)\mssqllocaldb called RazorPagesCoffee
* In the RazorPagesCoffee.Database project use LocalDB.scmp to create the tables
* Execute RecreateData.sql

### Live Demo Site ###
* http://razorpagescoffee.azurewebsites.net/Genres
Hosted on free Azure App Server in Western Europe with a 5DTU SQL Server DB.

### Who do I talk to? ###
* davemateer@gmail.com