﻿Delete from Movie
Go
Delete from Genre
Go
Delete from Review
Go


SET IDENTITY_INSERT [dbo].[Genre] ON 
GO
INSERT [dbo].[Genre] ([GenreID], [Name]) VALUES (1, N'comedy')
GO
INSERT [dbo].[Genre] ([GenreID], [Name]) VALUES (2, N'documentary')
GO
SET IDENTITY_INSERT [dbo].[Genre] OFF
GO