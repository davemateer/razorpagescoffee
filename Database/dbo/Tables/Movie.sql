﻿CREATE TABLE [dbo].[Movie] (
    [MovieID]     INT             IDENTITY (1, 1) NOT NULL,
    [GenreID]     INT             NOT NULL,
    [Price]       DECIMAL (18, 2) NOT NULL,
    [ReleaseDate] DATETIME2 (7)   NOT NULL,
    [Title]       NVARCHAR (MAX)  NOT NULL,
    CONSTRAINT [PK_Movie] PRIMARY KEY CLUSTERED ([MovieID] ASC),
    CONSTRAINT [FK_Movie_Genre_GenreID] FOREIGN KEY ([GenreID]) REFERENCES [dbo].[Genre] ([GenreID]) ON DELETE CASCADE
);

