﻿CREATE TABLE [dbo].[Review] (
    [ReviewID] INT            IDENTITY (1, 1) NOT NULL,
    [MovieID]  INT            NOT NULL,
    [Name]     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Review] PRIMARY KEY CLUSTERED ([ReviewID] ASC),
    CONSTRAINT [FK_Review_Movie_MovieID] FOREIGN KEY ([MovieID]) REFERENCES [dbo].[Movie] ([MovieID]) ON DELETE CASCADE
);

