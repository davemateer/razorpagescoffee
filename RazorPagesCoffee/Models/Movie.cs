﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace RazorPagesCoffee.Models
{
    public class Movie
    {
        public int MovieID { get; set; }
        [Required, DisplayName("Title of Movie")]
        public string Title { get; set; }
        [Display(Name = "Release Date"), DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }
        public decimal Price { get; set; }

        // 1 Movie can have only a single genre
        public int GenreID { get; set; }
        public Genre Genre { get; set; }

        // 1 Movie has many
        public List<Review> Reviews { get; set; }

    }

    public class Genre
    {
        public int GenreID { get; set; }
        public string Name { get; set; }
    }

    // Reviews of the Movie
    public class Review
    {
        public int ReviewID { get; set; }
        public string Name { get; set; }

        public int MovieID { get; set; }
        public Movie Movie { get; set; }
    }

}
