﻿using System;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace RazorPagesCoffee.Services
{
    public class DapperService : IDisposable
    {
        // Expression bodied member 
        public SqlConnection Connection => _connection ?? (_connection = GetOpenConnection());
        private static SqlConnection _connection;

        private static SqlConnection GetOpenConnection()
        {
            var connection = new SqlConnection(_connectionString);
            connection.Open();
            return connection;
        }

        private static string _connectionString = "";
        public DapperService(IConfiguration configuration) =>
            _connectionString = configuration.GetValue<string>("ConnectionStrings:DefaultConnection");

        public void Dispose() => _connection?.Dispose();
    }
}