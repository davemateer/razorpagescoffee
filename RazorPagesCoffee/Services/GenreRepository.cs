﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using RazorPagesCoffee.Models;

namespace RazorPagesCoffee.Services
{
    public class GenreRepository
    {
        private readonly IDbConnection db;
        public GenreRepository(DapperService dapperService) => db = dapperService.Connection;

        public IList<Genre> GetGenres() => db.Query<Genre>(@"Select * From Genre").ToList();
    }
}
