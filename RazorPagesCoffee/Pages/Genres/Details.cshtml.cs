using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RazorPagesCoffee;
using RazorPagesCoffee.Models;

namespace RazorPagesCoffee.Pages.Genres
{
    public class DetailsModel : PageModel
    {
        private readonly RazorPagesCoffee.TestContext _context;

        public DetailsModel(RazorPagesCoffee.TestContext context)
        {
            _context = context;
        }

        public Genre Genre { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Genre = await _context.Genre.SingleOrDefaultAsync(m => m.GenreID == id);

            if (Genre == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
