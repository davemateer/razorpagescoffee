using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RazorPagesCoffee.Models;
using RazorPagesCoffee.Services;

namespace RazorPagesCoffee.Pages.Genres
{
    public class IndexModel2 : PageModel
    {
        private readonly GenreRepository r;
        
        public IndexModel2 (GenreRepository r)
        {
            this.r = r;
        }

        public IList<Genre> Genres { get;set; }

        public void OnGet()
        {
            Genres = r.GetGenres();
        }
    }
}
